public class testAl1_7 {
    public static boolean PairSum(int A[], int c) {
        for (int i = 0; i < A.length - 1; i++) {
            for (int j = i + 1; j < A.length; j++) {
                if (A[i] + A[j] == c) {
                    System.out.println("Pair of sum " + c + " is (" + A[i] + ", " + A[j] + ")");
                    return true;
                }
            }
        }
        return false;
    }

    public static void main(String[] args) {
        int A[] = {8,7,5,4,2,6};
        int c = 8;

        PairSum(A, c);
    }
}