import java.util.Arrays;

public class testAl1_8 {
    public static void reverse(int arr[],int start,int end) {
        int temp;
        if (start >= end) {
            return;
        }
        temp = arr[start];
        arr[start] = arr[end];
        arr[end] = temp;
        reverse(arr, start+1, end-1);
    }
    
    public static void main(String [] args) {
        int arr[] = {8,7,5,4,2,6};

        reverse(arr, 0,arr.length-1);
        String strArr = Arrays.toString(arr);
        System.out.println(strArr);
    }
}
